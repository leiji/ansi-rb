#!/usr/bin/env ruby

# This library provides ANSI colorization for Ruby strings.
# It's a partial port of my ansi go library.
#
#	License (ISC)
#
#	Copyright (c) 2013, Alexander Kahl <alex@ifsr.de>
#
#	Permission to use, copy, modify, and/or distribute this software for any
#	purpose with or without fee is hereby granted, provided that the above
#	copyright notice and this permission notice appear in all copies.
#
#	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

module ANSI

	require 'strscan'

	ESC="\033"
	CSI=ESC+"["

	SGR_RESET                    = 0
	SGR_BOLD                     = 1
	SGR_FAINT                    = 2
	SGR_ITALIC_ON                = 3
	SGR_UNDERLINE                = 4
	SGR_BLINK_SLOW               = 5
	SGR_BLINK_RAPID              = 6
	SGR_IMAGE_NEGATIVE           = 7
	SGR_REVERSE                  = SGR_IMAGE_NEGATIVE
	SGR_CONCEAL                  = 8
	SGR_CROSSED_OUT              = 9
	SGR_PRIMARY_FONT             = 10
	SGR_ALTERNATE_FONT_1         = 11
	SGR_ALTERNATE_FONT_2         = 12
	SGR_ALTERNATE_FONT_3         = 13
	SGR_ALTERNATE_FONT_4         = 14
	SGR_ALTERNATE_FONT_5         = 15
	SGR_ALTERNATE_FONT_6         = 16
	SGR_ALTERNATE_FONT_7         = 17
	SGR_ALTERNATE_FONT_8         = 18
	SGR_ALTERNATE_FONT_9         = 19
	SGR_FRAKTUR                  = 20
	SGR_BOLD_OFF                 = 21
	SGR_UNDERLINE_DOUBLE         = SGR_BOLD_OFF
	SGR_NORMAL_INTENSITY         = 22
	SGR_NOT_ITALIC_NOT_FRAKTUR   = 23
	SGR_UNDERLINE_NONE           = 24
	SGR_BLINK_OFF                = 25
	SGR_RESERVED_26              = 26
	SGR_IMAGE_POSITIVE           = 27
	SGR_REVEAL                   = 28 # conceal off
	SGR_NOT_CROSSED_OUT          = 29
	SGR_FOREGROUND_BLACK         = 30
	SGR_FOREGROUND_RED           = 31
	SGR_FOREGROUND_GREEN         = 32
	SGR_FOREGROUND_YELLOW        = 33
	SGR_FOREGROUND_BLUE          = 34
	SGR_FOREGROUND_MAGENTA       = 35
	SGR_FOREGROUND_CYAN          = 36
	SGR_FOREGROUND_WHITE         = 37
	SGR_FOREGROUND_256           = 38
	SGR_FOREGROUND_DEFAULT       = 39
	SGR_BACKGROUND_BLACK         = 40
	SGR_BACKGROUND_RED           = 41
	SGR_BACKGROUND_GREEN         = 42
	SGR_BACKGROUND_YELLOW        = 43
	SGR_BACKGROUND_BLUE          = 44
	SGR_BACKGROUND_MAGENTA       = 45
	SGR_BACKGROUND_CYAN          = 46
	SGR_BACKGROUND_WHITE         = 47
	SGR_BACKGROUND_256           = 48
	SGR_BACKGROUND_DEFAULT       = 49
	SGR_RESERVED_50              = 50
	SGR_FRAMED                   = 51
	SGR_ENCIRCLED                = 52
	SGR_OVERLINED                = 53
	SGR_NOT_FRAMED_NOT_ENCIRCLED = 54
	SGR_NOT_OVERLINED            = 55
	SGR_RESERVED_56              = 56
	SGR_RESERVED_57              = 57
	SGR_RESERVED_58              = 58
	SGR_RESERVED_59              = 59

	Short = {
		"~"=> SGR_RESET,
		"!"=> SGR_BOLD,
		"_"=> SGR_UNDERLINE,
		"%"=> SGR_IMAGE_NEGATIVE,
		"&"=> SGR_IMAGE_POSITIVE,
		"."=> SGR_NORMAL_INTENSITY,
		"k"=> SGR_FOREGROUND_BLACK,
		"r"=> SGR_FOREGROUND_RED,
		"g"=> SGR_FOREGROUND_GREEN,
		"y"=> SGR_FOREGROUND_YELLOW,
		"b"=> SGR_FOREGROUND_BLUE,
		"m"=> SGR_FOREGROUND_MAGENTA,
		"c"=> SGR_FOREGROUND_CYAN,
		"w"=> SGR_FOREGROUND_WHITE,
		"d"=> SGR_FOREGROUND_DEFAULT,
		"K"=> SGR_BACKGROUND_BLACK,
		"R"=> SGR_BACKGROUND_RED,
		"G"=> SGR_BACKGROUND_GREEN,
		"Y"=> SGR_BACKGROUND_YELLOW,
		"B"=> SGR_BACKGROUND_BLUE,
		"M"=> SGR_BACKGROUND_MAGENTA,
		"C"=> SGR_BACKGROUND_CYAN,
		"W"=> SGR_BACKGROUND_WHITE,
		"D"=> SGR_BACKGROUND_DEFAULT
	}

	ESC_RUNE = '@'

	def ANSI.sgr(*codes)
		"#{CSI}#{codes.join(";")}m"
	end

	def ANSI.replace_color str
		s = StringScanner.new(str)

		# check for color codes
		unless s.exist? Regexp.new(ESC_RUNE) then
			# no color codes. return unmodified string
			return str
		end

		res = ""

		while not s.eos? do	
			char = s.getch
			
			# this isnt the char you are looking for *waves hand*
			unless char == ESC_RUNE then
				res << char
				next
			end

			
			nextchar = s.peek 1

			# string ends after ESC_RUNE? just add the char back to result
			if nextchar == "" then
				res << char
				next # and effectively exiting the loop
			end
			# is it starting a code group?
			if nextchar == "{" then
				# handle code group
				# test that the group is actually closed
				if not s.exist? /\}/ then
					# doesn't seem to be a valid code group, ignore it
					res << char
					next
				end
				s.getch # advance scanner
				codes = []
				loop do
					grpchar = s.getch
					break if grpchar == "}" # exit at the end of the group
					codes << Short[grpchar] if Short.has_key? grpchar
				end

				res << sgr(*codes) # splat!




			# is the next char a valid code?
			elsif Short.has_key? nextchar then
				res << sgr(Short[nextchar])
				s.getch # advance the pos
			else
				res << char # nextchar will be added in the next iteration (this allows us to have @@@@!)
			end

		end

		return res




	end

	def ANSI.print s
		$stdout.print replace_color(s)
	end


	def ANSI.puts s
		$stdout.puts replace_color(s)
	end
end

# You might want to use it like this:

#class String
#
#	def colorize
#		ANSI.replace_color self
#	end
#
#
#end
#
#
#puts "@!Hello @{~Rw}World@~".colorize
#

if $0 == __FILE__ then
	ANSI.puts ARGV.join(" ")
end

